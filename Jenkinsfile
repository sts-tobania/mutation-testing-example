#!groovy
pipeline {
    agent any
    stages {
        stage('Setup and Checkout') {
            steps {
                checkout scm
            }
        }

        stage('Unit Tests BackEnd') {
            steps {
                script {
                    docker.image('cimg/openjdk:17.0.9-node').inside(){
                        sh 'mvn clean test'
                    }
                }
            }
        }

        stage('Mutation Tests') {
            steps {
                script {
                    docker.image('cimg/openjdk:17.0.9-node').inside(){
                        sh 'mvn org.pitest:pitest-maven:mutationCoverage'
                    }
                }
            }
        }

        stage('Sonar Scan') {
            environment {
                scannerHome = tool 'SonarQubeScanner'
            }
            steps {
                withSonarQubeEnv('sonarqube') {
                    script {
                        docker.image('cimg/openjdk:17.0.9-node').inside(){
                            //sh 'mvn org.sonarsource.scanner.maven:sonar-maven-plugin:3.6.0.1398:sonar -Dsonar.projectName=MutationExample -Dsonar.inclusions=**/main/** -Dsonar.exclusions=**/test/**'
                            sh 'mvn verify sonar:sonar'
                        }
                    }
                }
            }
        }

        stage('Reports') {
            steps {
                jacoco sourceInclusionPattern: '**/*.java'

                publishHTML([allowMissing: false, alwaysLinkToLastBuild: false, keepAll: false, reportDir: 'target/pit-reports/', reportFiles: '*/index.html', reportName: 'Mutation Testing Report', reportTitles: ''])
            }
        }
    }
}

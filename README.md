About this repository
==================
This repository contains some code samples for mutation testing using pitest [https://pitest.org](https://pitest.org).

What do I need to run these samples?
---
A Java 8 JDK and Maven. That's it.

How do I run it?
---
Unit tests & mutation tests can be run locally: 
```
mvn clean test
mvn org.pitest:pitest-maven:mutationCoverage
```
Reports can be found in the target folder 

Can I run it in Jenkins?
---
Setup a Jenkins with SonarQube and use the Jenkinsfile for the build job.



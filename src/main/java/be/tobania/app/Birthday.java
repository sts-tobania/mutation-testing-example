package be.tobania.app;

public class Birthday {
    public static String birthdayMessage (int age) {
        celebrate(age);
        return "Happy Birthday!";
    }

    private static void celebrate(int age) {
        System.out.println("Happy birthday! You are " + age + " years old!");

    }
}

package be.tobania.app;

public class AgeSelector {
    public static String evaluate(int age) {
        if (age < 20) return "teenager";
        else return "old :-)";
    }
}


package be.tobania.app;

import be.tobania.app.AgeSelector;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class AgeSelectorTest {
    // Horrible (no) tests
    /*@Test
    public void testFor15_ReturnsTeenager() {
        be.tobania.app.AgeSelector.evaluate(15);
    }

    @Test
    public void testFor25_ReturnsOld() {
        be.tobania.app.AgeSelector.evaluate(25);
    }*/

    // Bad tests
   /* @Test
    public void testShouldReturnTeenager_For16() {
        assertEquals("teenager", be.tobania.app.AgeSelector.evaluate(16));
    }

    @Test
    public void testShouldReturnOld_For26() {
        assertEquals("old :-)", be.tobania.app.AgeSelector.evaluate(26));
    }*/

    // Good tests
    @Test
    public void testShouldReturnTeenager_For19() {
        assertEquals("teenager", AgeSelector.evaluate(19));
    }

    @Test
    public void testShouldReturnOld_For20() {
        assertEquals("old :-)", AgeSelector.evaluate(20));
    }

}

package be.tobania.app;

import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.Assert.assertEquals;

public class BirthdayTest {

    @Test
    public void testShouldReturnBirthdayMessage() {
        Birthday.birthdayMessage(25);
    }

    @Test
    public void testShouldReturnCorrectBirthdayMessage() {
        assertEquals("Happy Birthday!", Birthday.birthdayMessage(26));
    }

    @Test
    public void shouldPrintCorrectMessage() {
        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));

        Birthday.birthdayMessage(27);

        String expectedOutput  = "Happy birthday! You are 27 years old!\n";

        assertEquals(expectedOutput, outContent.toString());
    }
}
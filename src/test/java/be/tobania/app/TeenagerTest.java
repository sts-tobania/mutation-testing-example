package be.tobania.app;

import be.tobania.app.Teenager;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class TeenagerTest {

    @Test
    public void testFor15_ShouldReturnTrue() {
        Teenager.isTeenager(15);
    }

    @Test
public void testFor25_ShouldReturnFalse() {
        Teenager.isTeenager(25);
    }

    @Test
    public void testFor16_ShouldReturnTrue() {
        assertTrue(Teenager.isTeenager(16));
    }

    @Test
    public void testFor26_ShouldReturnFalse() {
        assertFalse(Teenager.isTeenager(26));
    }

    @Test
    public void testFor19_ShouldReturnTrue() {
        assertTrue(Teenager.isTeenager(19));
    }

    @Test
    public void testFor20_ShouldReturnFalse() {
        assertFalse(Teenager.isTeenager(20));
    }
}